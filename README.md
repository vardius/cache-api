contact API
================
Rafa� Lorenz <vardius@gmail.com>

ABOUT
==================================================
Project consist of two main directories `cmd` and `internal`.

`cmd` - main application, entry point for executable.
`internal` - common code, application framework, most likely to be extracted as common tool kit in the future.

Each service in `cmd` directory has it's own `internal` directory. We use `internal` to prevent exporting package's code from outside of its scope. Each executable code is divided into 4 layers:

1. Package infrastructure is a layer that holds everything that interacts with other systems - Secondary/Driven Adapters. Secondary adapters wrap around a tool and adapt its input/output to a port, which fits the application core needs and can simply be an interface or a complex set of interfaces and objects.
2. Package application is a layer responsible for driving the workflow of the application,
matching the use cases at hand. These operations are interface-independent and can be both synchronous or message-driven. This layer is well suited for spanning transactions, high-level logging and security.
The application layer is thin in terms of domain logic
it merely coordinates the domain layer objects to perform the actual work.
3. Package interfaces is a layer that holds everything that other systems interacts with - Primary/Driving Adapters.
Primary adapters wrap around a use case and adapt its input/output to a delivery mechanism, ie. HTTP/HTML, HTTP/JSON or CLI.
4. Domain - not relevant here as I skipped DDD part

For the simplicity of this application, contacts are stored in redis, project does not contain docker-compose file or kubernetes deployments. Redis connection is required for project to work.

Usually data validation would be done with the use of value objects. This way one can be sure the data passed behind handlers should always conform to the rules.

I tried to write comments whenever i remembered :). Please ask if anything doesn't make sense or needs to be clarified.

Project contains debug routes available at:

- `/debug/pprof` - Added to the default mux by importing the `net/http/pprof` package.
- `/debug/vars` - Added to the default mux by importing the `expvar` package.

EXAMPLE
==================================================
## Prerequisites
In order to run this project you need to have Docker > 1.17.05 for building the image.

## Quick start

### Makefile
```
version         Show version
docker-build    [DOCKER] Build given container. Example: `make docker-build BIN=contact`
docker-run      [DOCKER] Run container on given port. Example: `make docker-run BIN=contact PORT=3000 AUTOPILOT_API_KEY=change_me`
docker-rm       [DOCKER] Stop and then remove docker container. Example: `make docker-rm BIN=contact`
```

### Running service
```sh
make docker-build BIN=contact
make docker-run BIN=contact PORT=3000 AUTOPILOT_API_KEY=change_me 
```

for more env variables refer to the application config package [here](https://bitbucket.org/vardius/cache-api/src/8367ffa903a4844bcf5f6c71f6c5a683ff68bdc7/cmd/contact/internal/application/config/env.go#lines-13)

you might copy paste command from [Makefile](https://bitbucket.org/vardius/cache-api/Makefile), and override default values by passing `--env` or even `--env-file`

### Interacting with a service
#### Get contact
```sh
curl http://localhost:3000/contacts/{id}
```
#### Create contact via POST/PUT
```sh
curl -d '{"contact":{"FirstName":"Slarty","LastName":"Bartfast","Email":"test@slarty.com","custom":{"string--Test--Field":"This is a test"}}}' -H "Content-Type: application/json" -X POST http://localhost:3000/contacts
```
