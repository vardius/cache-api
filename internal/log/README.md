# log [![GoDoc](https://godoc.org/bitbucket.org/vardius/cache-api/internal/log?status.svg)](https://godoc.org/bitbucket.org/vardius/cache-api/internal/log)
Package log provides Logger

Download:
```shell
go get -u bitbucket.org/vardius/cache-api/internal/log
```

* * *
Package log provides Logger
