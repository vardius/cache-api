# response [![GoDoc](https://godoc.org/bitbucket.org/vardius/cache-api/internal/http/response?status.svg)](https://godoc.org/bitbucket.org/vardius/cache-api/internal/http/response)
Package response provides helpers and utils for working with HTTP response

Download:
```shell
go get -u bitbucket.org/vardius/cache-api/internal/http/response
```

* * *
Package response provides helpers and utils for working with HTTP response
