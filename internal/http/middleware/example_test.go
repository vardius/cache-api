package middleware_test

import (
	"net/http"
	"net/http/httptest"

	"bitbucket.org/vardius/cache-api/internal/http/middleware"
	"bitbucket.org/vardius/cache-api/internal/log"
)

func ExampleRecover() {
	m := middleware.Recover(log.New("development"))
	handler := m(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		panic("error")
	}))

	// We will mock request for this example
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/", nil)

	handler.ServeHTTP(w, req)
}

func ExampleLogger() {
	m := middleware.Logger(log.New("development"))
	h := m(http.HandlerFunc(func(_ http.ResponseWriter, _ *http.Request) {}))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/", nil)

	h.ServeHTTP(w, req)
}
