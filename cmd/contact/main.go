package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"

	config "bitbucket.org/vardius/cache-api/cmd/contact/internal/application/config"
	persistence_autopilot "bitbucket.org/vardius/cache-api/cmd/contact/internal/infrastructure/persistence/autopilot"
	persistence_redis "bitbucket.org/vardius/cache-api/cmd/contact/internal/infrastructure/persistence/redis"
	interface_http "bitbucket.org/vardius/cache-api/cmd/contact/internal/interfaces/http"
	application "bitbucket.org/vardius/cache-api/internal/application"
	buildinfo "bitbucket.org/vardius/cache-api/internal/buildinfo"
	log "bitbucket.org/vardius/cache-api/internal/log"
	redis "github.com/go-redis/redis/v7"
)

func main() {
	buildinfo.PrintVersionOrContinue()

	ctx := context.Background()

	logger := log.New(config.Env.APP.Environment)
	client := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%d", config.Env.Redis.Host, config.Env.Redis.Port),
	})
	repository := persistence_redis.NewContactRepository(
		client,
		config.Env.APP.CacheExpiration,
		persistence_autopilot.NewContactRepository(
			&http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true,
					},
				},
			},
			"https://api2.autopilothq.com",
			config.Env.Autopilot.APIKey,
			logger,
		),
		logger,
	)
	router := interface_http.NewRouter(logger, repository)
	app := application.New(logger)

	app.AddAdapters(
		interface_http.NewAdapter(
			fmt.Sprintf("%s:%d", config.Env.HTTP.Host, config.Env.HTTP.Port),
			router,
		),
	)

	if config.Env.APP.Environment == "development" {
		app.AddAdapters(
			application.NewDebugAdapter(
				fmt.Sprintf("%s:%d", config.Env.Debug.Host, config.Env.Debug.Port),
			),
		)
	}

	app.WithShutdownTimeout(config.Env.APP.ShutdownTimeout)
	app.Run(ctx)
}
