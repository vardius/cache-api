package persistence

import (
	"context"
	"encoding/json"
)

// Contact model
type Contact struct {
	ID string `json:"contact_id"`
}

// ContactRepository allows to get/save contact to storage
type ContactRepository interface {
	Get(ctx context.Context, id string) (json.RawMessage, error)
	Add(ctx context.Context, contact json.RawMessage) error
}
