package redis

import (
	"context"
	"encoding/json"
	"time"

	"bitbucket.org/vardius/cache-api/cmd/contact/internal/infrastructure/persistence"
	"bitbucket.org/vardius/cache-api/internal/errors"
	"bitbucket.org/vardius/cache-api/internal/log"
	"github.com/go-redis/redis/v7"
)

// NewContactRepository returns redis implementation
func NewContactRepository(client *redis.Client, expiration time.Duration, parentRepository persistence.ContactRepository, logger *log.Logger) persistence.ContactRepository {
	return &contactRepository{
		client:           client,
		expiration:       expiration,
		parentRepository: parentRepository,
		logger:           logger,
	}
}

type contactRepository struct {
	expiration       time.Duration
	client           *redis.Client
	parentRepository persistence.ContactRepository
	logger           *log.Logger
}

func (r *contactRepository) Get(ctx context.Context, id string) (json.RawMessage, error) {
	data, err := r.client.Get(id).Result()

	r.logger.Info(ctx, "[ContactRepository|Redis]: GET\nContactID: %s\n%s\nErr:%v\n", id, data, err)

	if err != nil {
		contact, err := r.parentRepository.Get(ctx, id)
		if err != nil {
			return nil, err
		}

		r.Add(ctx, contact)
		if err != nil {
			return nil, err
		}

		return contact, nil
	}

	return []byte(data), nil
}

func (r *contactRepository) Add(ctx context.Context, data json.RawMessage) error {
	var err error
	contact := persistence.Contact{}

	err = json.Unmarshal(data, &contact)
	if err != nil {
		return errors.Wrapf(err, errors.INTERNAL, "Error while trying to unmarshal payload %s", data)
	}

	err = r.parentRepository.Add(ctx, data)
	if err != nil {
		return err
	}

	err = r.client.Set(contact.ID, string(data), r.expiration).Err()

	r.logger.Info(ctx, "[ContactRepository|Redis]: SET\nContactID: %s\n%s\nErr:%v\n", contact.ID, data, err)

	if err != nil {
		return errors.Wrap(err, errors.INTERNAL, "Error storing contact in redis cache")
	}

	return nil
}
