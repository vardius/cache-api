package redis

import (
	"context"
	"encoding/json"
	"reflect"
	"testing"
	"time"

	"bitbucket.org/vardius/cache-api/internal/log"
	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis/v7"
)

var mockContact = []byte(`{
	"contact_id": "person_AP2-9cbf7ac0-eec5-11e4-87bc-6df09cc44d23",
	"FirstName": "Chris",
	"LastName": "Sharkey",
	"type": "Contact",
	"Email": "chris@autopilothq.com",
	"Phone": "4159945916",
	"created_at": "2015-04-29T23:15:25.347Z",
	"updated_at": "2015-04-29T23:15:25.347Z",
	"LeadSource": "Autopilot",
	"Status": "Testing",
	"Company": "Magpie API",
	"lists": [
	  "contactlist_9EAF39E4-9AEC-4134-964A"
	]
  }`)

type mockRepository struct{}

func (r *mockRepository) Get(ctx context.Context, id string) (json.RawMessage, error) {
	return mockContact, nil
}

func (r *mockRepository) Add(ctx context.Context, contact json.RawMessage) error {
	return nil
}

func TestAdd(t *testing.T) {
	s, err := miniredis.Run()
	if err != nil {
		t.Fatal(err)
	}
	defer s.Close()

	client := redis.NewClient(&redis.Options{
		Addr: s.Addr(),
	})

	r := NewContactRepository(client, 2*time.Second, &mockRepository{}, log.New("development"))

	t.Run("Should add mock contact", func(t *testing.T) {
		err := r.Add(context.Background(), mockContact)
		if err != nil {
			t.Error(err)
		}
	})
}

func TestGet(t *testing.T) {
	s, err := miniredis.Run()
	if err != nil {
		t.Fatal(err)
	}
	defer s.Close()

	client := redis.NewClient(&redis.Options{
		Addr: s.Addr(),
	})

	r := NewContactRepository(client, 2*time.Second, &mockRepository{}, log.New("development"))

	type args struct {
		ctx context.Context
		id  string
	}
	tests := []struct {
		name    string
		args    args
		want    json.RawMessage
		wantErr bool
	}{
		{"Should get mock contact from parent repository", args{context.Background(), "person_new"}, mockContact, false},
		{"Should get mock contact from cache", args{context.Background(), "person_AP2-9cbf7ac0-eec5-11e4-87bc-6df09cc44d23"}, mockContact, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := r.Get(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("contactRepository.Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("contactRepository.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}
