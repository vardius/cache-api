package redis

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/vardius/cache-api/internal/log"
)

func TestAdd(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if apiKey := req.Header.Get("autopilotapikey"); apiKey != "test" {
			t.Errorf("api key header does not match: got %v want %v", apiKey, "test")
		}

		if ctype := req.Header.Get("Content-Type"); ctype != "application/json" {
			t.Errorf("content type header does not match: got %v want %v", ctype, "application/json")
		}

		if req.Method != http.MethodPost {
			t.Errorf("method does not match: got %v want %v", req.Method, http.MethodPost)
		}

		w.WriteHeader(http.StatusOK)
	}))

	defer ts.Close()

	r := NewContactRepository(http.DefaultClient, ts.URL, "test", log.New("development"))

	t.Run("Should add mock contact", func(t *testing.T) {
		err := r.Add(context.Background(), []byte(`{"contact":{"FirstName":"Slarty","LastName":"Bartfast","Email":"test@slarty.com","custom":{"string--Test--Field":"This is a test"}}}`))
		if err != nil {
			t.Error(err)
		}
	})
}

func TestGet(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if apiKey := req.Header.Get("autopilotapikey"); apiKey != "test" {
			t.Errorf("api key header does not match: got %v want %v", apiKey, "test")
		}

		if req.Method != http.MethodGet {
			t.Errorf("method does not match: got %v want %v", req.Method, http.MethodGet)
		}

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{
			"contact_id": "person_AP2-9cbf7ac0-eec5-11e4-87bc-6df09cc44d23",
			"FirstName": "Chris",
			"LastName": "Sharkey",
			"type": "Contact",
			"Email": "chris@autopilothq.com",
			"Phone": "4159945916",
			"created_at": "2015-04-29T23:15:25.347Z",
			"updated_at": "2015-04-29T23:15:25.347Z",
			"LeadSource": "Autopilot",
			"Status": "Testing",
			"Company": "Magpie API",
			"lists": [
			  "contactlist_9EAF39E4-9AEC-4134-964A"
			]
		  }`))
	}))

	defer ts.Close()

	id := "person_AP2-9cbf7ac0-eec5-11e4-87bc-6df09cc44d23"

	r := NewContactRepository(http.DefaultClient, ts.URL, "test", log.New("development"))

	t.Run("Should get mock contact", func(t *testing.T) {
		_, err := r.Get(context.Background(), id)
		if err != nil {
			t.Error(err)
		}
	})
}
