package redis

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/vardius/cache-api/cmd/contact/internal/infrastructure/persistence"
	"bitbucket.org/vardius/cache-api/internal/errors"
	"bitbucket.org/vardius/cache-api/internal/log"
)

// NewContactRepository returns view model repository for Contact
func NewContactRepository(client *http.Client, host, apiKey string, logger *log.Logger) persistence.ContactRepository {
	return &contactRepository{
		client: client,
		host:   host,
		apiKey: apiKey,
		logger: logger,
	}
}

type contactRepository struct {
	host   string
	apiKey string
	client *http.Client
	logger *log.Logger
}

func (r *contactRepository) Get(ctx context.Context, id string) (json.RawMessage, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/v1/contact/%s", r.host, id), nil)
	if err != nil {
		return nil, errors.Wrap(err, errors.INTERNAL, "Could not build request object")
	}

	req.Header.Add("autopilotapikey", r.apiKey)

	resp, err := r.client.Do(req)

	r.logger.Info(ctx, "[ContactRepository|Autopilot]:\nReq: %v\nResp: %v\nErr:%v\n", req, resp, err)

	if err != nil {
		return nil, errors.Wrap(err, errors.INTERNAL, "Error while sending request to the server")
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, errors.INTERNAL, "Could not read response body")
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, errors.New(errors.NOTFOUND, string(body))
	}

	if resp.StatusCode == http.StatusForbidden {
		return nil, errors.New(errors.FORBIDDEN, string(body))
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(errors.INTERNAL, string(body))
	}

	return body, nil
}

func (r *contactRepository) Add(ctx context.Context, contact json.RawMessage) error {
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/v1/contact", r.host), bytes.NewBuffer(contact))
	if err != nil {
		return errors.Wrap(err, errors.INTERNAL, "Could not build request object")
	}

	req.Header.Add("autopilotapikey", r.apiKey)
	req.Header.Add("Content-Type", "application/json")

	resp, err := r.client.Do(req)

	r.logger.Info(ctx, "[ContactRepository|Autopilot]:\nReq: %v\nResp: %v\nErr:%v\n", req, resp, err)

	if err != nil {
		return errors.Wrap(err, errors.INTERNAL, "Error while sending request to the server")
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, errors.INTERNAL, "Could not read response body")
	}

	if resp.StatusCode == http.StatusForbidden {
		return errors.New(errors.FORBIDDEN, string(body))
	}

	if resp.StatusCode != http.StatusOK {
		return errors.New(errors.INTERNAL, string(body))
	}

	return nil
}
