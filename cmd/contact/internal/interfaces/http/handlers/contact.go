package handlers

import (
	"io/ioutil"
	"net/http"

	"bitbucket.org/vardius/cache-api/cmd/contact/internal/infrastructure/persistence"
	"bitbucket.org/vardius/cache-api/internal/errors"
	"bitbucket.org/vardius/cache-api/internal/http/response"
	"github.com/vardius/gorouter/v4/context"
)

// BuildCreateOrUpdateContactHandler creates or updates contact
func BuildCreateOrUpdateContactHandler(repository persistence.ContactRepository) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		var err error

		if r.Body == nil {
			response.RespondJSONError(r.Context(), w, ErrEmptyRequestBody)
			return
		}

		defer r.Body.Close()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondJSONError(r.Context(), w, errors.Wrap(err, errors.INTERNAL, "Invalid request body"))
			return
		}

		if err = repository.Add(r.Context(), body); err != nil {
			response.RespondJSONError(r.Context(), w, errors.Wrap(err, errors.INTERNAL, "Could not add contact to cache"))
			return
		}

		response.RespondJSON(r.Context(), w, nil, http.StatusCreated)
		return
	}

	return http.HandlerFunc(fn)
}

// BuildGetContactHandler gets contact from cache
func BuildGetContactHandler(repository persistence.ContactRepository) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		var err error

		if r.Body == nil {
			response.RespondJSONError(r.Context(), w, ErrEmptyRequestBody)
			return
		}

		params, ok := context.Parameters(r.Context())
		if !ok {
			response.RespondJSONError(r.Context(), w, ErrInvalidURLParams)
			return
		}

		contact, err := repository.Get(r.Context(), params.Value("id"))
		if err != nil {
			response.RespondJSONError(r.Context(), w, errors.Wrap(err, errors.NOTFOUND, "Contact not found"))
			return
		}

		response.RespondJSON(r.Context(), w, contact, http.StatusOK)
		return
	}

	return http.HandlerFunc(fn)
}
