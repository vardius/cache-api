package http

import (
	persistence "bitbucket.org/vardius/cache-api/cmd/contact/internal/infrastructure/persistence"
	handlers "bitbucket.org/vardius/cache-api/cmd/contact/internal/interfaces/http/handlers"
	http_middleware "bitbucket.org/vardius/cache-api/internal/http/middleware"
	http_metadata_middleware "bitbucket.org/vardius/cache-api/internal/http/middleware/metadata"
	log "bitbucket.org/vardius/cache-api/internal/log"
	gorouter "github.com/vardius/gorouter/v4"
)

// NewRouter provides new router
func NewRouter(logger *log.Logger, repository persistence.ContactRepository) gorouter.Router {
	// Global middleware
	router := gorouter.New(
		http_middleware.Recover(logger),
		http_metadata_middleware.WithMetadata(),
		http_middleware.Logger(logger),
	)

	router.POST("/contacts", handlers.BuildCreateOrUpdateContactHandler(repository))
	router.PUT("/contacts", handlers.BuildCreateOrUpdateContactHandler(repository))
	router.GET("/contacts/{id}", handlers.BuildGetContactHandler(repository))

	return router
}
