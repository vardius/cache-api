package config

import (
	"log"
	"time"

	"github.com/caarlos0/env/v6"
)

// Env stores environment values
var Env *environment

type environment struct {
	APP struct {
		Environment     string        `env:"ENV"                     envDefault:"development"`
		ShutdownTimeout time.Duration `env:"SERVER_SHUTDOWN_TIMEOUT" envDefault:"5s"`
		CacheExpiration time.Duration `env:"CACHE_EXPIRATION"        envDefault:"5s"`
	}
	Debug struct {
		Host string `env:"DEBUG_HOST"      envDefault:"0.0.0.0"`
		Port int    `env:"DEBUG_PORT_HTTP" envDefault:"4000"`
	}
	HTTP struct {
		Origins []string `env:"ORIGINS" envSeparator:"|"` // Origins should follow format: scheme "://" host [ ":" port ]

		Host string `env:"HTTP_HOST" envDefault:"0.0.0.0"`
		Port int    `env:"HTTP_PORT" envDefault:"3000"`

		ReadTimeout  time.Duration `env:"HTTP_SERVER_READ_TIMEOUT"     envDefault:"5s"`
		WriteTimeout time.Duration `env:"HTTP_SERVER_WRITE_TIMEOUT"    envDefault:"10s"`
		IdleTimeout  time.Duration `env:"HTTP_SERVER_SHUTDOWN_TIMEOUT" envDefault:"120s"`
	}
	Autopilot struct {
		APIKey string `env:"AUTOPILOT_API_KEY" envDefault:"change_me"`
	}
	Redis struct {
		Host string `env:"REDIS_HOST" envDefault:"0.0.0.0"`
		Port int    `env:"REDIS_PORT" envDefault:"6379"`
	}
}

func init() {
	Env = &environment{}

	if err := env.Parse(&Env.APP); err != nil {
		panic(err)
	}
	if err := env.Parse(&Env.Debug); err != nil {
		panic(err)
	}
	if err := env.Parse(&Env.HTTP); err != nil {
		panic(err)
	}
	if err := env.Parse(&Env.Autopilot); err != nil {
		panic(err)
	}

	log.Printf("Env: %v\n", Env)
}
