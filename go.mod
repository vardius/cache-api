module bitbucket.org/vardius/cache-api

go 1.13

require (
	github.com/alicebob/gopher-json v0.0.0-20180125190556-5a6b3ba71ee6 // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/caarlos0/env/v6 v6.1.0
	github.com/go-redis/redis/v7 v7.0.0-beta.4
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/google/uuid v1.1.1
	github.com/stretchr/testify v1.4.0
	github.com/vardius/golog v1.1.1
	github.com/vardius/gorouter/v4 v4.2.3
	github.com/vardius/shutdown v1.0.0
	github.com/vardius/trace v1.0.1
	github.com/yuin/gopher-lua v0.0.0-20190514113301-1cd887cd7036 // indirect
)
